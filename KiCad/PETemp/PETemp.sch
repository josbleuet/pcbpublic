EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 4
Title "Power"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TPS61097A-33DBVT:TPS61097A-33DBVT U1
U 1 1 61008D10
P 4850 3250
F 0 "U1" H 5400 3515 50  0000 C CNN
F 1 "TPS61097A-33DBVT" H 5400 3424 50  0000 C CNN
F 2 "TPS61097A-33DBVT:SOT95P280X145-5N" H 5800 3350 50  0001 L CNN
F 3 "http://www.ti.com/lit/gpn/tps61097a-33" H 5800 3250 50  0001 L CNN
F 4 "0.9Vin, 3.3Vout Boost Converter with Bypass Switch - 5nA Shutdown Current" H 5800 3150 50  0001 L CNN "Description"
F 5 "1.45" H 5800 3050 50  0001 L CNN "Height"
F 6 "Texas Instruments" H 5800 2950 50  0001 L CNN "Manufacturer_Name"
F 7 "TPS61097A-33DBVT" H 5800 2850 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "595-TPS61097A-33DBVT" H 5800 2750 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/TPS61097A-33DBVT?qs=sU0fTKI0LukBYbTC8cJTjA%3D%3D" H 5800 2650 50  0001 L CNN "Mouser Price/Stock"
F 10 "TPS61097A-33DBVT" H 5800 2550 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/tps61097a-33dbvt/texas-instruments?region=nac" H 5800 2450 50  0001 L CNN "Arrow Price/Stock"
	1    4850 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 61009CD9
P 4750 4050
F 0 "#PWR0101" H 4750 3800 50  0001 C CNN
F 1 "GND" H 4755 3877 50  0000 C CNN
F 2 "" H 4750 4050 50  0001 C CNN
F 3 "" H 4750 4050 50  0001 C CNN
	1    4750 4050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 6100A1C7
P 6900 3900
F 0 "#PWR0102" H 6900 3650 50  0001 C CNN
F 1 "GND" H 6905 3727 50  0000 C CNN
F 2 "" H 6900 3900 50  0001 C CNN
F 3 "" H 6900 3900 50  0001 C CNN
	1    6900 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3250 4850 3250
$Comp
L Device:L L1
U 1 1 6100ACD6
P 5400 2800
F 0 "L1" V 5590 2800 50  0000 C CNN
F 1 "4.7uH" V 5499 2800 50  0000 C CNN
F 2 "Inductor_SMD:L_0805_2012Metric" H 5400 2800 50  0001 C CNN
F 3 "~" H 5400 2800 50  0001 C CNN
F 4 "CKP20164R7M-T" V 5400 2800 50  0001 C CNN "PartNumber"
	1    5400 2800
	0    -1   -1   0   
$EndComp
Connection ~ 4750 3250
$Comp
L power:+BATT #PWR0103
U 1 1 6100DF06
P 2400 2750
F 0 "#PWR0103" H 2400 2600 50  0001 C CNN
F 1 "+BATT" H 2415 2923 50  0000 C CNN
F 2 "" H 2400 2750 50  0001 C CNN
F 3 "" H 2400 2750 50  0001 C CNN
	1    2400 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 6100EF06
P 2400 4100
F 0 "#PWR0104" H 2400 3850 50  0001 C CNN
F 1 "GND" H 2405 3927 50  0000 C CNN
F 2 "" H 2400 4100 50  0001 C CNN
F 3 "" H 2400 4100 50  0001 C CNN
	1    2400 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 610176E8
P 6900 3600
F 0 "C2" H 7015 3646 50  0000 L CNN
F 1 "10uF" H 7015 3555 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 6938 3450 50  0001 C CNN
F 3 "~" H 6900 3600 50  0001 C CNN
	1    6900 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 3750 6900 3900
Wire Wire Line
	5950 3350 6150 3350
Wire Wire Line
	6150 3350 6150 2800
Wire Wire Line
	6150 2800 5550 2800
Wire Wire Line
	5250 2800 4750 2800
Wire Wire Line
	4750 2800 4750 3250
$Comp
L power:GND #PWR0105
U 1 1 61039008
P 4300 4050
F 0 "#PWR0105" H 4300 3800 50  0001 C CNN
F 1 "GND" H 4305 3877 50  0000 C CNN
F 2 "" H 4300 4050 50  0001 C CNN
F 3 "" H 4300 4050 50  0001 C CNN
	1    4300 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 6103900E
P 4300 3750
F 0 "C1" H 4415 3796 50  0000 L CNN
F 1 "10uF" H 4415 3705 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 4338 3600 50  0001 C CNN
F 3 "~" H 4300 3750 50  0001 C CNN
	1    4300 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3900 4300 4050
$Sheet
S 3600 5100 1050 550 
U 6104190B
F0 "MCU" 50
F1 "MCU.sch" 50
$EndSheet
$Sheet
S 6400 5100 1050 550 
U 6105FDE6
F0 "RF" 50
F1 "RF.sch" 50
$EndSheet
$Sheet
S 5000 5100 1050 550 
U 61066303
F0 "TC" 50
F1 "TC.sch" 50
$EndSheet
Text GLabel 3950 3450 0    50   Input ~ 0
ENABLE_3V3
Wire Wire Line
	8650 2000 9250 2000
Wire Wire Line
	8450 1550 8000 1550
Wire Wire Line
	8450 1700 8450 1550
$Comp
L Transistor_FET:AO3401A Q1
U 1 1 610ECD53
P 8450 1900
F 0 "Q1" V 8699 1900 50  0000 C CNN
F 1 "SSM3J355R,LF" V 8790 1900 50  0000 C CNN
F 2 "SSM3J355R,LF:SSM3J355R-LF" H 8650 1825 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=55743&prodName=SSM3J355R" H 8450 1900 50  0001 L CNN
	1    8450 1900
	0    1    1    0   
$EndComp
Text GLabel 8000 1550 0    50   Input ~ 0
RFPOWERUP
Wire Wire Line
	2400 2750 2400 3250
Connection ~ 2400 3250
$Comp
L power:VCC #PWR0106
U 1 1 610F2A49
P 6900 2750
F 0 "#PWR0106" H 6900 2600 50  0001 C CNN
F 1 "VCC" H 6915 2923 50  0000 C CNN
F 2 "" H 6900 2750 50  0001 C CNN
F 3 "" H 6900 2750 50  0001 C CNN
	1    6900 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 2750 6900 3250
Connection ~ 6900 3250
Wire Wire Line
	6900 3250 6900 3450
Text GLabel 9250 2000 2    50   Output ~ 0
VCC_RF
Wire Wire Line
	8650 2950 9250 2950
Wire Wire Line
	8450 2500 8000 2500
Wire Wire Line
	8450 2650 8450 2500
$Comp
L Transistor_FET:AO3401A Q2
U 1 1 6110132C
P 8450 2850
F 0 "Q2" V 8699 2850 50  0000 C CNN
F 1 "SSM3J355R,LF" V 8790 2850 50  0000 C CNN
F 2 "SSM3J355R,LF:SSM3J355R-LF" H 8650 2775 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=55743&prodName=SSM3J355R" H 8450 2850 50  0001 L CNN
	1    8450 2850
	0    1    1    0   
$EndComp
Text GLabel 8000 2500 0    50   Input ~ 0
MAX_VCC
Text GLabel 9250 2950 2    50   Output ~ 0
VCC_TC
Wire Wire Line
	8650 4000 9250 4000
Wire Wire Line
	8450 3550 8000 3550
Wire Wire Line
	8450 3700 8450 3550
$Comp
L Transistor_FET:AO3401A Q3
U 1 1 611029B4
P 8450 3900
F 0 "Q3" V 8699 3900 50  0000 C CNN
F 1 "SSM3J355R,LF" V 8790 3900 50  0000 C CNN
F 2 "SSM3J355R,LF:SSM3J355R-LF" H 8650 3825 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=55743&prodName=SSM3J355R" H 8450 3900 50  0001 L CNN
	1    8450 3900
	0    1    1    0   
$EndComp
Text GLabel 8000 3550 0    50   Input ~ 0
VCC_ANA_ON
Text GLabel 9250 4000 2    50   Output ~ 0
VCC_ANA
Wire Wire Line
	6900 3250 7350 3250
Wire Wire Line
	7350 3250 7350 2950
Wire Wire Line
	7350 2000 8250 2000
Wire Wire Line
	8250 2950 7350 2950
Connection ~ 7350 2950
Wire Wire Line
	7350 2950 7350 2000
Wire Wire Line
	8250 4000 7350 4000
Wire Wire Line
	7350 4000 7350 3250
Connection ~ 7350 3250
Connection ~ 4300 3250
Wire Wire Line
	4300 3250 4750 3250
Wire Wire Line
	2400 3250 4300 3250
Wire Wire Line
	4300 3250 4300 3600
Wire Wire Line
	4850 3350 4750 3350
Wire Wire Line
	4750 3350 4750 4050
Wire Wire Line
	3950 3450 4850 3450
Wire Wire Line
	2400 3250 2400 3500
Wire Wire Line
	2400 3600 2400 4100
$Comp
L Connector:Conn_01x02_Male J?
U 1 1 612997AB
P 1850 3500
AR Path="/6104190B/612997AB" Ref="J?"  Part="1" 
AR Path="/612997AB" Ref="J12"  Part="1" 
F 0 "J12" H 1958 3681 50  0000 C CNN
F 1 "Conn_01x02_Male" H 1958 3590 50  0000 C CNN
F 2 "KEYSTONE2462:BatteryHolder_Keystone_2462_2xAA" H 1850 3500 50  0001 C CNN
F 3 "~" H 1850 3500 50  0001 C CNN
	1    1850 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 3500 2050 3500
Wire Wire Line
	2400 3600 2050 3600
Text Notes 1450 3600 0    50   ~ 0
Batteries
$Comp
L 865080142007:865080142007 C41
U 1 1 613DC4D0
P 6350 3400
F 0 "C41" V 6554 3530 50  0000 L CNN
F 1 "100uF 6.3V" V 6645 3530 50  0000 L CNN
F 2 "BasicFootprint:C_1206_3216Metric" H 6700 3450 50  0001 L CNN
F 3 "https://katalog.we-online.com/pbs/datasheet/865080142007.pdf" H 6700 3350 50  0001 L CNN
F 4 "Wurth Elektronik 100uF 25 V dc Aluminium Electrolytic Capacitor, WCAP-ASLI Series 2000h 5 (Dia.) x 5.35mm" H 6700 3250 50  0001 L CNN "Description"
F 5 "5.5" H 6700 3150 50  0001 L CNN "Height"
F 6 "Wurth Elektronik" H 6700 3050 50  0001 L CNN "Manufacturer_Name"
F 7 "865080142007" H 6700 2950 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "710-865080142007" H 6700 2850 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Wurth-Elektronik/865080142007/?qs=0KOYDY2FL2%2Fwso3YmuIP2w%3D%3D" H 6700 2750 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 6700 2650 50  0001 L CNN "Arrow Part Number"
F 11 "" H 6700 2550 50  0001 L CNN "Arrow Price/Stock"
	1    6350 3400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 613DC4D6
P 6350 4050
AR Path="/6104190B/613DC4D6" Ref="#PWR?"  Part="1" 
AR Path="/6105FDE6/613DC4D6" Ref="#PWR?"  Part="1" 
AR Path="/613DC4D6" Ref="#PWR0113"  Part="1" 
F 0 "#PWR0113" H 6350 3800 50  0001 C CNN
F 1 "GND" H 6355 3877 50  0000 C CNN
F 2 "" H 6350 4050 50  0001 C CNN
F 3 "" H 6350 4050 50  0001 C CNN
	1    6350 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 4050 6350 3900
Wire Wire Line
	6350 3250 6350 3400
Wire Wire Line
	5950 3250 6350 3250
Connection ~ 6350 3250
Wire Wire Line
	6350 3250 6900 3250
$EndSCHEMATC
