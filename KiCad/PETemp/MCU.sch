EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 2 4
Title "MCU"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ATXMEGA128A1U-AU:ATXMEGA128A1U-AU U2
U 1 1 6104209A
P 3850 4000
AR Path="/6104190B/6104209A" Ref="U2"  Part="1" 
AR Path="/6105FDE6/6104209A" Ref="U?"  Part="1" 
AR Path="/61066303/6104209A" Ref="U?"  Part="1" 
F 0 "U2" H 3850 6870 50  0000 C CNN
F 1 "ATXMEGA128A1U-AU" H 3850 6779 50  0000 C CNN
F 2 "ATXMEGA128A1U-AU:QFP50P1600X1600X120-100N" H 3850 4000 50  0001 L BNN
F 3 "" H 3850 4000 50  0001 L BNN
F 4 "Microchip" H 3850 4000 50  0001 L BNN "MF"
F 5 "MCU 8-bit/16-bit XMEGA AVR RISC 128KB Flash 1.8V/2.5V/3.3V 100-Pin TQFP" H 3850 4000 50  0001 L BNN "DESCRIPTION"
F 6 "Unavailable" H 3850 4000 50  0001 L BNN "AVAILABILITY"
F 7 "TQFP-100 Microchip" H 3850 4000 50  0001 L BNN "PACKAGE"
F 8 "IPC-7351B" H 3850 4000 50  0001 L BNN "STANDARD"
F 9 "ATXMEGA128A1U-AU" H 3850 4000 50  0001 L BNN "MP"
F 10 "None" H 3850 4000 50  0001 L BNN "PRICE"
	1    3850 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 610489E9
P 6650 1350
AR Path="/6104190B/610489E9" Ref="C5"  Part="1" 
AR Path="/6105FDE6/610489E9" Ref="C?"  Part="1" 
AR Path="/61066303/610489E9" Ref="C?"  Part="1" 
F 0 "C5" H 6765 1396 50  0000 L CNN
F 1 "0.1uF" H 6765 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6688 1200 50  0001 C CNN
F 3 "~" H 6650 1350 50  0001 C CNN
	1    6650 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 6106675E
P 7150 1350
AR Path="/6104190B/6106675E" Ref="C6"  Part="1" 
AR Path="/6105FDE6/6106675E" Ref="C?"  Part="1" 
AR Path="/61066303/6106675E" Ref="C?"  Part="1" 
F 0 "C6" H 7265 1396 50  0000 L CNN
F 1 "0.1uF" H 7265 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7188 1200 50  0001 C CNN
F 3 "~" H 7150 1350 50  0001 C CNN
	1    7150 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 6106675F
P 7650 1350
AR Path="/6104190B/6106675F" Ref="C7"  Part="1" 
AR Path="/6105FDE6/6106675F" Ref="C?"  Part="1" 
AR Path="/61066303/6106675F" Ref="C?"  Part="1" 
F 0 "C7" H 7765 1396 50  0000 L CNN
F 1 "0.1uF" H 7765 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7688 1200 50  0001 C CNN
F 3 "~" H 7650 1350 50  0001 C CNN
	1    7650 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 61066760
P 8150 1350
AR Path="/6104190B/61066760" Ref="C8"  Part="1" 
AR Path="/6105FDE6/61066760" Ref="C?"  Part="1" 
AR Path="/61066303/61066760" Ref="C?"  Part="1" 
F 0 "C8" H 8265 1396 50  0000 L CNN
F 1 "0.1uF" H 8265 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8188 1200 50  0001 C CNN
F 3 "~" H 8150 1350 50  0001 C CNN
	1    8150 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 61066761
P 8650 1350
AR Path="/6104190B/61066761" Ref="C9"  Part="1" 
AR Path="/6105FDE6/61066761" Ref="C?"  Part="1" 
AR Path="/61066303/61066761" Ref="C?"  Part="1" 
F 0 "C9" H 8765 1396 50  0000 L CNN
F 1 "0.1uF" H 8765 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8688 1200 50  0001 C CNN
F 3 "~" H 8650 1350 50  0001 C CNN
	1    8650 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 61066762
P 9150 1350
AR Path="/6104190B/61066762" Ref="C10"  Part="1" 
AR Path="/6105FDE6/61066762" Ref="C?"  Part="1" 
AR Path="/61066303/61066762" Ref="C?"  Part="1" 
F 0 "C10" H 9265 1396 50  0000 L CNN
F 1 "0.1uF" H 9265 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9188 1200 50  0001 C CNN
F 3 "~" H 9150 1350 50  0001 C CNN
	1    9150 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C11
U 1 1 6104A666
P 9650 1350
AR Path="/6104190B/6104A666" Ref="C11"  Part="1" 
AR Path="/6105FDE6/6104A666" Ref="C?"  Part="1" 
AR Path="/61066303/6104A666" Ref="C?"  Part="1" 
F 0 "C11" H 9765 1396 50  0000 L CNN
F 1 "0.1uF" H 9765 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9688 1200 50  0001 C CNN
F 3 "~" H 9650 1350 50  0001 C CNN
	1    9650 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C12
U 1 1 61066764
P 10150 1350
AR Path="/6104190B/61066764" Ref="C12"  Part="1" 
AR Path="/6105FDE6/61066764" Ref="C?"  Part="1" 
AR Path="/61066303/61066764" Ref="C?"  Part="1" 
F 0 "C12" H 10265 1396 50  0000 L CNN
F 1 "0.1uF" H 10265 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 10188 1200 50  0001 C CNN
F 3 "~" H 10150 1350 50  0001 C CNN
	1    10150 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 61066765
P 6150 1350
AR Path="/6104190B/61066765" Ref="C4"  Part="1" 
AR Path="/6105FDE6/61066765" Ref="C?"  Part="1" 
AR Path="/61066303/61066765" Ref="C?"  Part="1" 
F 0 "C4" H 6265 1396 50  0000 L CNN
F 1 "0.1uF" H 6265 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6188 1200 50  0001 C CNN
F 3 "~" H 6150 1350 50  0001 C CNN
	1    6150 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 61066766
P 5650 1350
AR Path="/6104190B/61066766" Ref="C3"  Part="1" 
AR Path="/6105FDE6/61066766" Ref="C?"  Part="1" 
AR Path="/61066303/61066766" Ref="C?"  Part="1" 
F 0 "C3" H 5765 1396 50  0000 L CNN
F 1 "0.1uF" H 5765 1305 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5688 1200 50  0001 C CNN
F 3 "~" H 5650 1350 50  0001 C CNN
	1    5650 1350
	1    0    0    -1  
$EndComp
Text Notes 6150 800  0    50   ~ 0
Mettre chaque condensateur près d'une pin VCC et son GND
Wire Wire Line
	10150 1200 10150 1050
Wire Wire Line
	10150 1050 9650 1050
Wire Wire Line
	9650 1050 9650 1200
Wire Wire Line
	9650 1050 9150 1050
Wire Wire Line
	9150 1050 9150 1200
Connection ~ 9650 1050
Wire Wire Line
	9150 1050 8650 1050
Wire Wire Line
	8650 1050 8650 1200
Connection ~ 9150 1050
Wire Wire Line
	8650 1050 8150 1050
Wire Wire Line
	8150 1050 8150 1200
Connection ~ 8650 1050
Wire Wire Line
	8150 1050 7650 1050
Wire Wire Line
	7650 1050 7650 1200
Connection ~ 8150 1050
Wire Wire Line
	7650 1050 7150 1050
Wire Wire Line
	7150 1050 7150 1200
Connection ~ 7650 1050
Wire Wire Line
	7150 1050 6650 1050
Wire Wire Line
	6650 1050 6650 1200
Connection ~ 7150 1050
Wire Wire Line
	6650 1050 6150 1050
Wire Wire Line
	6150 1050 6150 1200
Connection ~ 6650 1050
Wire Wire Line
	6150 1050 5650 1050
Wire Wire Line
	5650 1050 5650 1200
Connection ~ 6150 1050
Wire Wire Line
	10150 1500 10150 1650
Wire Wire Line
	10150 1650 9650 1650
Wire Wire Line
	9650 1650 9650 1500
Wire Wire Line
	9650 1650 9150 1650
Wire Wire Line
	9150 1650 9150 1500
Connection ~ 9650 1650
Wire Wire Line
	9150 1650 8650 1650
Wire Wire Line
	5650 1650 5650 1500
Connection ~ 9150 1650
Wire Wire Line
	6150 1500 6150 1650
Connection ~ 6150 1650
Wire Wire Line
	6150 1650 5650 1650
Wire Wire Line
	6650 1500 6650 1650
Connection ~ 6650 1650
Wire Wire Line
	6650 1650 6150 1650
Wire Wire Line
	7150 1500 7150 1650
Connection ~ 7150 1650
Wire Wire Line
	7150 1650 6650 1650
Wire Wire Line
	7650 1500 7650 1650
Connection ~ 7650 1650
Wire Wire Line
	7650 1650 7150 1650
Wire Wire Line
	8150 1500 8150 1650
Connection ~ 8150 1650
Wire Wire Line
	8150 1650 7650 1650
Wire Wire Line
	8650 1500 8650 1650
Connection ~ 8650 1650
Wire Wire Line
	8650 1650 8150 1650
$Comp
L power:GND #PWR0107
U 1 1 61066767
P 10150 1850
AR Path="/6104190B/61066767" Ref="#PWR0107"  Part="1" 
AR Path="/6105FDE6/61066767" Ref="#PWR?"  Part="1" 
AR Path="/61066303/61066767" Ref="#PWR?"  Part="1" 
F 0 "#PWR0107" H 10150 1600 50  0001 C CNN
F 1 "GND" H 10155 1677 50  0000 C CNN
F 2 "" H 10150 1850 50  0001 C CNN
F 3 "" H 10150 1850 50  0001 C CNN
	1    10150 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 900  5650 1050
Connection ~ 5650 1050
Wire Wire Line
	10150 1850 10150 1650
Connection ~ 10150 1650
$Comp
L power:GND #PWR0108
U 1 1 61066769
P 5050 6700
AR Path="/6104190B/61066769" Ref="#PWR0108"  Part="1" 
AR Path="/6105FDE6/61066769" Ref="#PWR?"  Part="1" 
AR Path="/61066303/61066769" Ref="#PWR?"  Part="1" 
F 0 "#PWR0108" H 5050 6450 50  0001 C CNN
F 1 "GND" H 5055 6527 50  0000 C CNN
F 2 "" H 5050 6700 50  0001 C CNN
F 3 "" H 5050 6700 50  0001 C CNN
	1    5050 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 6700 5050 6500
Wire Wire Line
	5050 900  5050 1400
Wire Wire Line
	5050 1500 4750 1500
Wire Wire Line
	4750 1400 5050 1400
Connection ~ 5050 1400
Wire Wire Line
	5050 1400 5050 1500
Wire Wire Line
	4750 6500 5050 6500
Text GLabel 2350 2600 0    50   Output ~ 0
ENABLE_3V3
$Comp
L AB26TRB-32.768KHZ-T:AB26TRB-32.768KHZ-T XTAL1
U 1 1 610202BF
P 7000 5700
F 0 "XTAL1" H 7000 5937 50  0000 C CNN
F 1 "AB26TRB-32.768KHZ-T" H 7000 5846 50  0000 C CNN
F 2 "AB26TRQ-32:XTAL_AB26TRB-32.768KHZ-T" H 7000 5700 50  0001 L BNN
F 3 "" H 7000 5700 50  0001 L BNN
F 4 "Manufacturer recommendation" H 7000 5700 50  0001 L BNN "STANDARD"
F 5 "ABRACON Corporation" H 7000 5700 50  0001 L BNN "MANUFACTURER"
F 6 "04.24.13" H 7000 5700 50  0001 L BNN "PARTREV"
	1    7000 5700
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 610D1280
P 9450 6250
AR Path="/6104190B/610D1280" Ref="#PWR0109"  Part="1" 
AR Path="/6105FDE6/610D1280" Ref="#PWR?"  Part="1" 
AR Path="/61066303/610D1280" Ref="#PWR?"  Part="1" 
F 0 "#PWR0109" H 9450 6000 50  0001 C CNN
F 1 "GND" H 9455 6077 50  0000 C CNN
F 2 "" H 9450 6250 50  0001 C CNN
F 3 "" H 9450 6250 50  0001 C CNN
	1    9450 6250
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 610D1286
P 9450 5950
AR Path="/61066303/610D1286" Ref="R?"  Part="1" 
AR Path="/6104190B/610D1286" Ref="R2"  Part="1" 
F 0 "R2" H 9518 5996 50  0000 L CNN
F 1 "10k" H 9518 5905 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 9450 5950 50  0001 C CNN
F 3 "~" H 9450 5950 50  0001 C CNN
	1    9450 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 6050 9450 6250
$Comp
L Device:R_Small_US R?
U 1 1 610D21F4
P 9450 5500
AR Path="/61066303/610D21F4" Ref="R?"  Part="1" 
AR Path="/6104190B/610D21F4" Ref="R1"  Part="1" 
F 0 "R1" H 9518 5546 50  0000 L CNN
F 1 "30k" H 9518 5455 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 9450 5500 50  0001 C CNN
F 3 "~" H 9450 5500 50  0001 C CNN
	1    9450 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 5100 9450 5400
Wire Wire Line
	2950 2600 2350 2600
Text GLabel 2350 2100 0    50   Input ~ 0
BATT_SENSE
Wire Wire Line
	9000 5750 9450 5750
Wire Wire Line
	9450 5600 9450 5750
Connection ~ 9450 5750
Wire Wire Line
	9450 5750 9450 5850
$Comp
L power:VCC #PWR0110
U 1 1 610F93B1
P 5650 900
F 0 "#PWR0110" H 5650 750 50  0001 C CNN
F 1 "VCC" H 5665 1073 50  0000 C CNN
F 2 "" H 5650 900 50  0001 C CNN
F 3 "" H 5650 900 50  0001 C CNN
	1    5650 900 
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0111
U 1 1 610FA180
P 5050 900
F 0 "#PWR0111" H 5050 750 50  0001 C CNN
F 1 "VCC" H 5065 1073 50  0000 C CNN
F 2 "" H 5050 900 50  0001 C CNN
F 3 "" H 5050 900 50  0001 C CNN
	1    5050 900 
	1    0    0    -1  
$EndComp
Text GLabel 9000 5100 0    50   Input ~ 0
VCC_ANA
Wire Wire Line
	9000 5100 9450 5100
Text Notes 1050 9000 0    50   ~ 0
(ICI J'AVAIS MIS UN MAX qq chose pour prévoir de faire des tests avec une sonde RTD) ignorer cela...\n#define RTD_DVDD IOPORT_CREATE_PIN(PORTA, 4) //RTD DVDD\n#define RTD_AVDD IOPORT_CREATE_PIN(PORTA, 5) //RTD AVDD\n#define RTD_DRDY IOPORT_CREATE_PIN(PORTC, O) //RTD DATA READT\n#define RTD_SPISEL IOPORT_CREATE_PIN(PORTF, O) //SPI SEL\n\n\n(ICI C'EST POUR LE RFM95, devrait etre direct replacement de l'ancien module RF)\n#define RFRX    IOPORT_CREATE_PIN(PORTF, 0) (ANTENNE EN MODE RX)\n#define RFTX    IOPORT_CREATE_PIN(PORTF, 1) (ANTENNE EN MODE TX)\n\n\n#define RTC_CLOCK_OUT IOPORT_CREATE_PIN(PORTA, 7) //diagnostiquer la rtc clock au scope (UN PAD QU'ON PEUT SOUDER)\n\n#define BATLEVEL_1v_REFERENCE IOPORT_CREATE_PIN(PORTA, 0) //Calissé au ground, pour lire en differentiel.. errata. Ne sert pas parce que je prend le 1v interne.. ne pas utiliser.. spare\n#define BATLEVEL_SENSE IOPORT_CREATE_PIN(PORTA, 1) //Attention ne sert pas en IO\n#define BATLEVEL_CALIB_PIN IOPORT_CREATE_PIN(PORTA, 2)\n#define USB_IO IOPORT_CREATE_PIN(PORTA, 3)\n\n\n#define PCINTERRUPT   IOPORT_CREATE_PIN(PORTH, 1) (Mettre sur un pad a souder)\n\nIci c'es le chip que j'ai utiliser et que j'aimerais remetre, lit ambiante au 0.1 degré celcius, calibré en usine, j'avais utilisé un board qui integrait le chip que j'ai soudé a bras sur mon board)\n#define SI7051POWERUP IOPORT_CREATE_PIN(PORTC, 6)\n
Wire Wire Line
	5350 2300 4750 2300
Text GLabel 5350 2300 2    50   Output ~ 0
RFPOWERUP
Wire Wire Line
	5350 2400 4750 2400
Text GLabel 5350 2400 2    50   Output ~ 0
RFNSEL
Wire Wire Line
	5350 2500 4750 2500
Text GLabel 5350 2500 2    50   Output ~ 0
RFSDI
Wire Wire Line
	5350 2600 4750 2600
Text GLabel 5350 2600 2    50   Input ~ 0
RFSDO
Wire Wire Line
	5350 2700 4750 2700
Text GLabel 5350 2700 2    50   Output ~ 0
RFSCK
Wire Wire Line
	5350 2200 4750 2200
Text GLabel 5350 2200 2    50   Output ~ 0
RFRESET
Wire Wire Line
	5350 3800 4750 3800
Text GLabel 5350 3800 2    50   Output ~ 0
RFGPIO0
Wire Wire Line
	5350 3900 4750 3900
Wire Wire Line
	5350 4000 4750 4000
Wire Wire Line
	5350 4100 4750 4100
Wire Wire Line
	5350 4200 4750 4200
Wire Wire Line
	5350 4300 4750 4300
Text GLabel 5350 3900 2    50   Output ~ 0
RFGPIO1
Text GLabel 5350 4000 2    50   Output ~ 0
RFGPIO2
Text GLabel 5350 4100 2    50   Output ~ 0
RFGPIO3
Text GLabel 5350 4200 2    50   Output ~ 0
RFGPIO4
Text GLabel 5350 4300 2    50   Output ~ 0
RFGPIO5
$Comp
L Connector:Conn_01x04_Male J3
U 1 1 61134320
P 1200 3100
F 0 "J3" H 1300 2800 50  0000 C CNN
F 1 "Conn_01x04_Male" H 1300 2700 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 1200 3100 50  0001 C CNN
F 3 "~" H 1200 3100 50  0001 C CNN
	1    1200 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2900 2350 2900
Text GLabel 2350 2900 0    50   Input ~ 0
MAX_READY
Wire Wire Line
	2950 5800 2350 5800
Text GLabel 2350 5900 0    50   Output ~ 0
MAX_VCC
Wire Wire Line
	2950 5900 2350 5900
Text GLabel 2350 5800 0    50   Input ~ 0
MAX_FAULT
Wire Wire Line
	2950 6200 2350 6200
Text GLabel 2350 6300 0    50   Output ~ 0
MAX_SCK
Wire Wire Line
	2950 6300 2350 6300
Wire Wire Line
	2950 6000 2350 6000
Wire Wire Line
	2950 6100 2350 6100
Text GLabel 2350 6100 0    50   Output ~ 0
MAX_SDI
Text GLabel 2350 6000 0    50   Output ~ 0
MAX_CS
Text GLabel 2350 6200 0    50   Input ~ 0
MAX_SDO
Text GLabel 2350 4800 0    50   Output ~ 0
SI7051SCL
Text GLabel 2350 4700 0    50   Output ~ 0
SI7051SDA
Text GLabel 9000 5750 0    50   Output ~ 0
BATT_SENSE
Wire Wire Line
	2950 2100 2350 2100
Wire Wire Line
	2350 4700 2950 4700
Wire Wire Line
	2350 4800 2950 4800
Text GLabel 2350 2200 0    50   Output ~ 0
VCC_ANA_ON
Wire Wire Line
	2950 2200 2350 2200
NoConn ~ 6800 5700
NoConn ~ 7200 5700
NoConn ~ 2950 2000
NoConn ~ 2950 2300
NoConn ~ 2950 2400
NoConn ~ 2950 2500
NoConn ~ 4750 2000
NoConn ~ 4750 2100
NoConn ~ 4750 2900
NoConn ~ 4750 3000
NoConn ~ 4750 3100
NoConn ~ 4750 3200
NoConn ~ 4750 3300
NoConn ~ 4750 3400
NoConn ~ 4750 3500
NoConn ~ 4750 3600
NoConn ~ 2950 3800
NoConn ~ 2950 3900
NoConn ~ 2950 4000
NoConn ~ 2950 4100
NoConn ~ 2950 4200
NoConn ~ 2950 4300
NoConn ~ 2950 4400
NoConn ~ 2950 4500
NoConn ~ 2950 4900
NoConn ~ 2950 5000
NoConn ~ 2950 5100
NoConn ~ 2950 5200
NoConn ~ 2950 5300
NoConn ~ 2950 5400
NoConn ~ 2950 5600
NoConn ~ 2950 5700
NoConn ~ 4750 6200
NoConn ~ 4750 6100
NoConn ~ 4750 5900
NoConn ~ 4750 5800
NoConn ~ 4750 5700
NoConn ~ 4750 5600
NoConn ~ 4750 5400
NoConn ~ 4750 5300
NoConn ~ 4750 5200
NoConn ~ 4750 5100
NoConn ~ 4750 5000
NoConn ~ 4750 4900
NoConn ~ 4750 4800
NoConn ~ 4750 4700
NoConn ~ 4750 4500
NoConn ~ 4750 4400
$Comp
L Device:C C14
U 1 1 611A5039
P 7350 5950
AR Path="/6104190B/611A5039" Ref="C14"  Part="1" 
AR Path="/6105FDE6/611A5039" Ref="C?"  Part="1" 
AR Path="/61066303/611A5039" Ref="C?"  Part="1" 
F 0 "C14" H 7465 5996 50  0000 L CNN
F 1 "18pF" H 7465 5905 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 7388 5800 50  0001 C CNN
F 3 "~" H 7350 5950 50  0001 C CNN
	1    7350 5950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C13
U 1 1 611A503F
P 6650 5950
AR Path="/6104190B/611A503F" Ref="C13"  Part="1" 
AR Path="/6105FDE6/611A503F" Ref="C?"  Part="1" 
AR Path="/61066303/611A503F" Ref="C?"  Part="1" 
F 0 "C13" H 6765 5996 50  0000 L CNN
F 1 "18pF" H 6765 5905 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 6688 5800 50  0001 C CNN
F 3 "~" H 6650 5950 50  0001 C CNN
	1    6650 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 611A9F73
P 6650 6250
AR Path="/6104190B/611A9F73" Ref="#PWR0122"  Part="1" 
AR Path="/6105FDE6/611A9F73" Ref="#PWR?"  Part="1" 
AR Path="/61066303/611A9F73" Ref="#PWR?"  Part="1" 
F 0 "#PWR0122" H 6650 6000 50  0001 C CNN
F 1 "GND" H 6655 6077 50  0000 C CNN
F 2 "" H 6650 6250 50  0001 C CNN
F 3 "" H 6650 6250 50  0001 C CNN
	1    6650 6250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0123
U 1 1 611AC24F
P 7350 6250
AR Path="/6104190B/611AC24F" Ref="#PWR0123"  Part="1" 
AR Path="/6105FDE6/611AC24F" Ref="#PWR?"  Part="1" 
AR Path="/61066303/611AC24F" Ref="#PWR?"  Part="1" 
F 0 "#PWR0123" H 7350 6000 50  0001 C CNN
F 1 "GND" H 7355 6077 50  0000 C CNN
F 2 "" H 7350 6250 50  0001 C CNN
F 3 "" H 7350 6250 50  0001 C CNN
	1    7350 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 5700 6650 5700
Wire Wire Line
	6650 5800 6650 5700
Connection ~ 6650 5700
Wire Wire Line
	6650 5700 6800 5700
Wire Wire Line
	7200 5700 7350 5700
Wire Wire Line
	7350 5700 7350 5800
Wire Wire Line
	7350 6100 7350 6250
Wire Wire Line
	6650 6100 6650 6250
Wire Wire Line
	7350 5700 7350 5600
Wire Wire Line
	7350 5600 4750 5600
Connection ~ 7350 5700
$Comp
L Connector:Conn_01x04_Male J5
U 1 1 611D06F4
P 1250 4300
F 0 "J5" H 1358 4481 50  0000 C CNN
F 1 "Conn_01x04_Male" H 1358 4390 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 1250 4300 50  0001 C CNN
F 3 "~" H 1250 4300 50  0001 C CNN
	1    1250 4300
	1    0    0    -1  
$EndComp
NoConn ~ 2950 3000
Wire Wire Line
	1400 3200 2950 3200
Wire Wire Line
	1400 3100 2950 3100
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J4
U 1 1 611F3AD8
P 8550 3450
F 0 "J4" H 8600 3867 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 8600 3776 50  0000 C CNN
F 2 "TC2050-IDC-NL:Tag-Connect_TC2050-IDC-NL_2x05_P1.27mm_Vertical" H 8550 3450 50  0001 C CNN
F 3 "~" H 8550 3450 50  0001 C CNN
	1    8550 3450
	1    0    0    -1  
$EndComp
Text Notes 8700 2850 2    59   ~ 0
JTAG
Text GLabel 7950 3250 0    50   Input ~ 0
JTAG_1
Text GLabel 7950 3350 0    50   Input ~ 0
JTAG_3
Text GLabel 7950 3450 0    50   Input ~ 0
JTAG_5
Text GLabel 7950 3550 0    50   Input ~ 0
ENABLE_3V3
Text GLabel 7950 3650 0    50   Input ~ 0
JTAG_9
Text GLabel 9300 3450 2    50   Input ~ 0
XMEGA_RESET
Text GLabel 9300 3550 2    50   Input ~ 0
PDI_DATA
Wire Wire Line
	8350 3250 7950 3250
Wire Wire Line
	7950 3350 8350 3350
Wire Wire Line
	8350 3450 7950 3450
Wire Wire Line
	7950 3550 8350 3550
Wire Wire Line
	8350 3650 7950 3650
Wire Wire Line
	9300 3550 8850 3550
Wire Wire Line
	8850 3450 9300 3450
Text GLabel 2550 3500 0    50   Input ~ 0
JTAG_1
Wire Wire Line
	2950 3500 2550 3500
Text GLabel 2550 3600 0    50   Input ~ 0
JTAG_3
Wire Wire Line
	2950 3600 2550 3600
Text GLabel 2550 3300 0    50   Input ~ 0
JTAG_5
Wire Wire Line
	2950 3300 2550 3300
Text GLabel 2550 3400 0    50   Input ~ 0
JTAG_9
Wire Wire Line
	2950 3400 2550 3400
$Comp
L power:GND #PWR0124
U 1 1 612758D4
P 10050 3350
AR Path="/6104190B/612758D4" Ref="#PWR0124"  Part="1" 
AR Path="/6105FDE6/612758D4" Ref="#PWR?"  Part="1" 
AR Path="/61066303/612758D4" Ref="#PWR?"  Part="1" 
F 0 "#PWR0124" H 10050 3100 50  0001 C CNN
F 1 "GND" H 10055 3177 50  0000 C CNN
F 2 "" H 10050 3350 50  0001 C CNN
F 3 "" H 10050 3350 50  0001 C CNN
	1    10050 3350
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0125
U 1 1 6127C83D
P 9800 3100
F 0 "#PWR0125" H 9800 2950 50  0001 C CNN
F 1 "VCC" H 9815 3273 50  0000 C CNN
F 2 "" H 9800 3100 50  0001 C CNN
F 3 "" H 9800 3100 50  0001 C CNN
	1    9800 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	10050 3250 10050 3350
Wire Wire Line
	8850 3250 10050 3250
Wire Wire Line
	9800 3350 9800 3100
Wire Wire Line
	8850 3350 9800 3350
$Comp
L Device:R_Small_US R?
U 1 1 61287BE2
P 2550 1300
AR Path="/61066303/61287BE2" Ref="R?"  Part="1" 
AR Path="/6104190B/61287BE2" Ref="R3"  Part="1" 
F 0 "R3" H 2618 1346 50  0000 L CNN
F 1 "30k" H 2618 1255 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 2550 1300 50  0001 C CNN
F 3 "~" H 2550 1300 50  0001 C CNN
	1    2550 1300
	1    0    0    -1  
$EndComp
Text GLabel 2200 1700 0    50   Input ~ 0
XMEGA_RESET
$Comp
L power:VCC #PWR0126
U 1 1 612963AE
P 2550 900
F 0 "#PWR0126" H 2550 750 50  0001 C CNN
F 1 "VCC" H 2565 1073 50  0000 C CNN
F 2 "" H 2550 900 50  0001 C CNN
F 3 "" H 2550 900 50  0001 C CNN
	1    2550 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 900  2550 1200
Wire Wire Line
	2200 1700 2550 1700
Wire Wire Line
	2550 1400 2550 1700
Connection ~ 2550 1700
Wire Wire Line
	2550 1700 2950 1700
Text GLabel 2200 1800 0    50   Input ~ 0
PDI_DATA
Wire Wire Line
	2200 1800 2950 1800
$Comp
L power:GND #PWR0127
U 1 1 612C3C7A
P 9100 3750
AR Path="/6104190B/612C3C7A" Ref="#PWR0127"  Part="1" 
AR Path="/6105FDE6/612C3C7A" Ref="#PWR?"  Part="1" 
AR Path="/61066303/612C3C7A" Ref="#PWR?"  Part="1" 
F 0 "#PWR0127" H 9100 3500 50  0001 C CNN
F 1 "GND" H 9105 3577 50  0000 C CNN
F 2 "" H 9100 3750 50  0001 C CNN
F 3 "" H 9100 3750 50  0001 C CNN
	1    9100 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 3650 9100 3650
Wire Wire Line
	9100 3650 9100 3750
$Comp
L Connector:Conn_01x03_Male J6
U 1 1 6125DFC5
P 6000 2900
F 0 "J6" H 6100 3150 50  0000 C CNN
F 1 "Conn_01x03_Male" H 6108 2990 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6000 2900 50  0001 C CNN
F 3 "~" H 6000 2900 50  0001 C CNN
	1    6000 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 2900 6200 2900
Wire Wire Line
	4750 3000 6200 3000
$Comp
L power:GND #PWR0128
U 1 1 6148025C
P 700 3000
F 0 "#PWR0128" H 700 2750 50  0001 C CNN
F 1 "GND" H 600 3000 50  0000 C CNN
F 2 "" H 700 3000 50  0001 C CNN
F 3 "" H 700 3000 50  0001 C CNN
	1    700  3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	700  3000 1400 3000
$Comp
L power:VCC #PWR0129
U 1 1 614852E1
P 700 3300
F 0 "#PWR0129" H 700 3150 50  0001 C CNN
F 1 "VCC" H 600 3300 50  0000 C CNN
F 2 "" H 700 3300 50  0001 C CNN
F 3 "" H 700 3300 50  0001 C CNN
	1    700  3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	700  3300 1400 3300
Wire Wire Line
	2950 4200 1450 4200
Wire Wire Line
	2950 4300 1450 4300
Wire Wire Line
	2950 4400 1450 4400
Wire Wire Line
	2950 4500 1450 4500
$Comp
L power:GND #PWR?
U 1 1 614C9492
P 6600 2800
F 0 "#PWR?" H 6600 2550 50  0001 C CNN
F 1 "GND" H 6605 2627 50  0000 C CNN
F 2 "" H 6600 2800 50  0001 C CNN
F 3 "" H 6600 2800 50  0001 C CNN
	1    6600 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 2800 6200 2800
$EndSCHEMATC
