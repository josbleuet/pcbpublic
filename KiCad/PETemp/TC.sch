EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 4 4
Title "TC"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C4
U 1 1 6104AC3D
P 4750 5950
AR Path="/6104190B/6104AC3D" Ref="C4"  Part="1" 
AR Path="/6105FDE6/6104AC3D" Ref="C?"  Part="1" 
AR Path="/61066303/6104AC3D" Ref="C32"  Part="1" 
F 0 "C32" V 4900 5900 50  0000 L CNN
F 1 "0.1uF" V 5000 5900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4788 5800 50  0001 C CNN
F 3 "~" H 4750 5950 50  0001 C CNN
	1    4750 5950
	0    1    1    0   
$EndComp
Text GLabel 6250 5000 0    50   Input ~ 0
MAX_SCK
Text GLabel 6250 5300 0    50   Output ~ 0
MAX_SDO
Text GLabel 6250 5200 0    50   Input ~ 0
MAX_SDI
Text GLabel 6250 5100 0    50   Input ~ 0
MAX_CS
$Comp
L PCC-SMP-K:PCC-SMP-K J1
U 1 1 61021BF6
P 2300 5600
AR Path="/61021BF6" Ref="J1"  Part="1" 
AR Path="/61066303/61021BF6" Ref="J1"  Part="1" 
F 0 "J1" H 2750 5865 50  0000 C CNN
F 1 "PCC-SMP-K" H 2750 5774 50  0000 C CNN
F 2 "PCC-SMP-K:PCCSMPK" H 3050 5700 50  0001 L CNN
F 3 "http://www.newportus.com/PDFspecs/PCC.pdf" H 3050 5600 50  0001 L CNN
F 4 "NEWPORT ELECTRONICS - PCC-SMP-K - Thermocouple Connector, PCB, Miniature, Type K, Socket" H 3050 5500 50  0001 L CNN "Description"
F 5 "6.3" H 3050 5400 50  0001 L CNN "Height"
F 6 "NEWPORT ELECTRONICS" H 3050 5300 50  0001 L CNN "Manufacturer_Name"
F 7 "PCC-SMP-K" H 3050 5200 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 3050 5100 50  0001 L CNN "Mouser Part Number"
F 9 "" H 3050 5000 50  0001 L CNN "Mouser Price/Stock"
F 10 "" H 3050 4900 50  0001 L CNN "Arrow Part Number"
F 11 "" H 3050 4800 50  0001 L CNN "Arrow Price/Stock"
	1    2300 5600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61030621
P 8700 6000
AR Path="/6104190B/61030621" Ref="#PWR?"  Part="1" 
AR Path="/6105FDE6/61030621" Ref="#PWR?"  Part="1" 
AR Path="/61066303/61030621" Ref="#PWR0116"  Part="1" 
F 0 "#PWR0116" H 8700 5750 50  0001 C CNN
F 1 "GND" H 8705 5827 50  0000 C CNN
F 2 "" H 8700 6000 50  0001 C CNN
F 3 "" H 8700 6000 50  0001 C CNN
	1    8700 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 5800 8700 5800
Wire Wire Line
	8700 5800 8700 5900
Wire Wire Line
	8400 5900 8700 5900
Connection ~ 8700 5900
Wire Wire Line
	8700 5900 8700 6000
Wire Wire Line
	3500 5500 3500 5700
Wire Wire Line
	3500 5700 3200 5700
Wire Wire Line
	4900 5950 4900 5500
Connection ~ 4900 5500
Wire Wire Line
	4900 5500 3500 5500
Wire Wire Line
	4600 5950 4600 5600
Connection ~ 4600 5600
Wire Wire Line
	4600 5600 3200 5600
Text GLabel 8650 5000 2    50   Output ~ 0
MAX_FAULT
Text GLabel 8650 5200 2    50   Output ~ 0
MAX_READY
Wire Wire Line
	6250 5300 6600 5300
Wire Wire Line
	6250 5200 6600 5200
Wire Wire Line
	6250 5100 6600 5100
Wire Wire Line
	6250 5000 6600 5000
Connection ~ 5800 5600
Connection ~ 6300 5500
Wire Wire Line
	6300 5500 4900 5500
Wire Wire Line
	6600 5500 6300 5500
Wire Wire Line
	5800 5600 4600 5600
Wire Wire Line
	6600 5600 6550 5600
Wire Wire Line
	5800 5600 5800 5800
Wire Wire Line
	5800 6100 5800 6250
Wire Wire Line
	6300 6100 6300 6250
Wire Wire Line
	6300 5500 6300 5800
$Comp
L power:GND #PWR?
U 1 1 61041794
P 5800 6250
AR Path="/6104190B/61041794" Ref="#PWR?"  Part="1" 
AR Path="/6105FDE6/61041794" Ref="#PWR?"  Part="1" 
AR Path="/61066303/61041794" Ref="#PWR0117"  Part="1" 
F 0 "#PWR0117" H 5800 6000 50  0001 C CNN
F 1 "GND" H 5805 6077 50  0000 C CNN
F 2 "" H 5800 6250 50  0001 C CNN
F 3 "" H 5800 6250 50  0001 C CNN
	1    5800 6250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61040A04
P 6300 6250
AR Path="/6104190B/61040A04" Ref="#PWR?"  Part="1" 
AR Path="/6105FDE6/61040A04" Ref="#PWR?"  Part="1" 
AR Path="/61066303/61040A04" Ref="#PWR0118"  Part="1" 
F 0 "#PWR0118" H 6300 6000 50  0001 C CNN
F 1 "GND" H 6305 6077 50  0000 C CNN
F 2 "" H 6300 6250 50  0001 C CNN
F 3 "" H 6300 6250 50  0001 C CNN
	1    6300 6250
	1    0    0    -1  
$EndComp
$Comp
L MAX31856MUD_:MAX31856MUD+ U3
U 1 1 610687B3
P 7500 5200
F 0 "U3" H 7500 6170 50  0000 C CNN
F 1 "MAX31856MUD+" H 7500 6079 50  0000 C CNN
F 2 "MAX31856MUD+:SOP65P640X110-14N" H 7500 5200 50  0001 L BNN
F 3 "" H 7500 5200 50  0001 L BNN
F 4 "Maxim Integrated" H 7500 5200 50  0001 L BNN "MF"
F 5 "Precision Thermocouple to Digital Converter 14-Pin TSSOP Tube" H 7500 5200 50  0001 L BNN "DESCRIPTION"
F 6 "Good" H 7500 5200 50  0001 L BNN "AVAILABILITY"
F 7 "5464" H 7500 5200 50  0001 L BNN "SNAPEDA_PACKAGE_ID"
F 8 "TSSOP-14 Maxim Integrated" H 7500 5200 50  0001 L BNN "PACKAGE"
F 9 "MAX31856MUD+" H 7500 5200 50  0001 L BNN "MP"
F 10 "4.88 USD" H 7500 5200 50  0001 L BNN "PRICE"
	1    7500 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 6104AF78
P 6300 5950
AR Path="/6104190B/6104AF78" Ref="C3"  Part="1" 
AR Path="/6105FDE6/6104AF78" Ref="C?"  Part="1" 
AR Path="/61066303/6104AF78" Ref="C34"  Part="1" 
F 0 "C34" H 6415 5996 50  0000 L CNN
F 1 "0.1uF" H 6415 5905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6338 5800 50  0001 C CNN
F 3 "~" H 6300 5950 50  0001 C CNN
	1    6300 5950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 610489E9
P 5800 5950
AR Path="/6104190B/610489E9" Ref="C5"  Part="1" 
AR Path="/6105FDE6/610489E9" Ref="C?"  Part="1" 
AR Path="/61066303/610489E9" Ref="C33"  Part="1" 
F 0 "C33" H 5915 5996 50  0000 L CNN
F 1 "0.1uF" H 5915 5905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5838 5800 50  0001 C CNN
F 3 "~" H 5800 5950 50  0001 C CNN
	1    5800 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 5000 9350 5150
Wire Wire Line
	9850 5000 9850 5150
$Comp
L power:GND #PWR?
U 1 1 6109F7C4
P 9350 5150
AR Path="/6104190B/6109F7C4" Ref="#PWR?"  Part="1" 
AR Path="/6105FDE6/6109F7C4" Ref="#PWR?"  Part="1" 
AR Path="/61066303/6109F7C4" Ref="#PWR0119"  Part="1" 
F 0 "#PWR0119" H 9350 4900 50  0001 C CNN
F 1 "GND" H 9355 4977 50  0000 C CNN
F 2 "" H 9350 5150 50  0001 C CNN
F 3 "" H 9350 5150 50  0001 C CNN
	1    9350 5150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6109F7CA
P 9850 5150
AR Path="/6104190B/6109F7CA" Ref="#PWR?"  Part="1" 
AR Path="/6105FDE6/6109F7CA" Ref="#PWR?"  Part="1" 
AR Path="/61066303/6109F7CA" Ref="#PWR0120"  Part="1" 
F 0 "#PWR0120" H 9850 4900 50  0001 C CNN
F 1 "GND" H 9855 4977 50  0000 C CNN
F 2 "" H 9850 5150 50  0001 C CNN
F 3 "" H 9850 5150 50  0001 C CNN
	1    9850 5150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6109F7D0
P 9850 4850
AR Path="/6104190B/6109F7D0" Ref="C?"  Part="1" 
AR Path="/6105FDE6/6109F7D0" Ref="C?"  Part="1" 
AR Path="/61066303/6109F7D0" Ref="C31"  Part="1" 
F 0 "C31" H 9965 4896 50  0000 L CNN
F 1 "0.1uF" H 9965 4805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9888 4700 50  0001 C CNN
F 3 "~" H 9850 4850 50  0001 C CNN
	1    9850 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 6109F7D6
P 9350 4850
AR Path="/6104190B/6109F7D6" Ref="C?"  Part="1" 
AR Path="/6105FDE6/6109F7D6" Ref="C?"  Part="1" 
AR Path="/61066303/6109F7D6" Ref="C30"  Part="1" 
F 0 "C30" H 9465 4896 50  0000 L CNN
F 1 "0.1uF" H 9465 4805 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9388 4700 50  0001 C CNN
F 3 "~" H 9350 4850 50  0001 C CNN
	1    9350 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 5000 8400 5000
Wire Wire Line
	8650 5200 8400 5200
Wire Wire Line
	9850 4500 9850 4600
Connection ~ 9850 4500
Connection ~ 9350 4600
Wire Wire Line
	9350 4600 9350 4700
Wire Wire Line
	9350 4600 9850 4600
Connection ~ 9850 4600
Wire Wire Line
	9850 4600 9850 4700
Wire Wire Line
	8400 4500 9850 4500
Wire Wire Line
	8400 4600 9350 4600
Wire Wire Line
	8400 4700 8700 4700
Wire Wire Line
	8700 4700 8700 4050
Wire Wire Line
	8700 4050 6550 4050
Wire Wire Line
	6550 4050 6550 5600
Connection ~ 6550 5600
Wire Wire Line
	6550 5600 5800 5600
Text GLabel 9700 4050 0    50   Input ~ 0
VCC_TC
Wire Wire Line
	9700 4050 9850 4050
Wire Wire Line
	9850 4050 9850 4500
$Comp
L Sensor_Temperature:Si7051-A20 U5
U 1 1 6110DFC9
P 7100 2450
F 0 "U5" H 7444 2496 50  0000 L CNN
F 1 "Si7051-A20" H 7444 2405 50  0000 L CNN
F 2 "Si7051:SON100P300X300X80-7N-D" H 7100 2050 50  0001 C CNN
F 3 "https://www.silabs.com/documents/public/data-sheets/Si7050-1-3-4-5-A20.pdf" H 6900 2750 50  0001 C CNN
	1    7100 2450
	1    0    0    -1  
$EndComp
NoConn ~ 2300 5600
NoConn ~ 2300 5700
Wire Wire Line
	8350 2600 8350 2750
$Comp
L Device:C C?
U 1 1 611103AA
P 8350 2450
AR Path="/6104190B/611103AA" Ref="C?"  Part="1" 
AR Path="/6105FDE6/611103AA" Ref="C?"  Part="1" 
AR Path="/61066303/611103AA" Ref="C35"  Part="1" 
F 0 "C35" H 8465 2496 50  0000 L CNN
F 1 "0.1uF" H 8465 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8388 2300 50  0001 C CNN
F 3 "~" H 8350 2450 50  0001 C CNN
	1    8350 2450
	1    0    0    -1  
$EndComp
Text GLabel 8200 1850 0    50   Input ~ 0
VCC_TC
Wire Wire Line
	8200 1850 8350 1850
$Comp
L power:GND #PWR?
U 1 1 61112B2A
P 8350 2950
AR Path="/6104190B/61112B2A" Ref="#PWR?"  Part="1" 
AR Path="/6105FDE6/61112B2A" Ref="#PWR?"  Part="1" 
AR Path="/61066303/61112B2A" Ref="#PWR0121"  Part="1" 
F 0 "#PWR0121" H 8350 2700 50  0001 C CNN
F 1 "GND" H 8355 2777 50  0000 C CNN
F 2 "" H 8350 2950 50  0001 C CNN
F 3 "" H 8350 2950 50  0001 C CNN
	1    8350 2950
	1    0    0    -1  
$EndComp
Text GLabel 5650 2350 0    50   Input ~ 0
SI7051SCL
Text GLabel 5650 2450 0    50   Input ~ 0
SI7051SDA
Wire Wire Line
	7100 2150 8350 2150
Wire Wire Line
	8350 2150 8350 1850
Wire Wire Line
	8350 2150 8350 2300
Connection ~ 8350 2150
Wire Wire Line
	7100 2750 8350 2750
Wire Wire Line
	8350 2750 8350 2950
Connection ~ 8350 2750
Wire Wire Line
	5650 2350 5950 2350
Wire Wire Line
	5650 2450 6400 2450
$Comp
L Device:R_Small_US R30
U 1 1 612D6748
P 5950 1900
AR Path="/61066303/612D6748" Ref="R30"  Part="1" 
AR Path="/6104190B/612D6748" Ref="R?"  Part="1" 
F 0 "R30" H 6018 1946 50  0000 L CNN
F 1 "10k" H 6018 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 5950 1900 50  0001 C CNN
F 3 "~" H 5950 1900 50  0001 C CNN
	1    5950 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R31
U 1 1 612D7357
P 6400 1900
AR Path="/61066303/612D7357" Ref="R31"  Part="1" 
AR Path="/6104190B/612D7357" Ref="R?"  Part="1" 
F 0 "R31" H 6468 1946 50  0000 L CNN
F 1 "10k" H 6468 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" H 6400 1900 50  0001 C CNN
F 3 "~" H 6400 1900 50  0001 C CNN
	1    6400 1900
	1    0    0    -1  
$EndComp
Text GLabel 5800 1550 0    50   Input ~ 0
VCC_TC
Wire Wire Line
	5800 1550 5950 1550
Wire Wire Line
	6400 1550 6400 1800
Wire Wire Line
	6400 2000 6400 2450
Connection ~ 6400 2450
Wire Wire Line
	6400 2450 6700 2450
Wire Wire Line
	5950 1800 5950 1550
Connection ~ 5950 1550
Wire Wire Line
	5950 1550 6400 1550
Wire Wire Line
	5950 2000 5950 2350
Connection ~ 5950 2350
Wire Wire Line
	5950 2350 6700 2350
$EndSCHEMATC
