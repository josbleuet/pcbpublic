EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 3 4
Title "RF"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C C3
U 1 1 6104AF78
P 5550 3150
AR Path="/6104190B/6104AF78" Ref="C3"  Part="1" 
AR Path="/6105FDE6/6104AF78" Ref="C40"  Part="1" 
F 0 "C40" H 5665 3196 50  0000 L CNN
F 1 "0.1uF" H 5665 3105 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric" H 5588 3000 50  0001 C CNN
F 3 "~" H 5550 3150 50  0001 C CNN
	1    5550 3150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 61050233
P 5550 3550
AR Path="/6104190B/61050233" Ref="#PWR?"  Part="1" 
AR Path="/6105FDE6/61050233" Ref="#PWR0112"  Part="1" 
F 0 "#PWR0112" H 5550 3300 50  0001 C CNN
F 1 "GND" H 5555 3377 50  0000 C CNN
F 2 "" H 5550 3550 50  0001 C CNN
F 3 "" H 5550 3550 50  0001 C CNN
	1    5550 3550
	1    0    0    -1  
$EndComp
$Comp
L RFM95W-915S2:RFM95W-915S2 U4
U 1 1 6106461E
P 3650 3550
AR Path="/6106461E" Ref="U4"  Part="1" 
AR Path="/6105FDE6/6106461E" Ref="U4"  Part="1" 
F 0 "U4" H 3650 4617 50  0000 C CNN
F 1 "RFM95W-915S2" H 3650 4526 50  0000 C CNN
F 2 "RFM95W-915S2:XCVR_RFM95W-915S2" H 3650 3550 50  0001 L BNN
F 3 "" H 3650 3550 50  0001 L BNN
F 4 "RF Solutions" H 3650 3550 50  0001 L BNN "MF"
F 5 "RFM95W LoRA Transceiver Module 915MHz" H 3650 3550 50  0001 L BNN "DESCRIPTION"
F 6 "Unavailable" H 3650 3550 50  0001 L BNN "AVAILABILITY"
F 7 "SMD-16 RF Solutions" H 3650 3550 50  0001 L BNN "PACKAGE"
F 8 "Manufacturer recommendations" H 3650 3550 50  0001 L BNN "STANDARD"
F 9 "RFM95W-915S2" H 3650 3550 50  0001 L BNN "MP"
F 10 "None" H 3650 3550 50  0001 L BNN "PRICE"
	1    3650 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3300 5550 3550
Wire Wire Line
	4350 2750 5550 2750
Wire Wire Line
	5550 3000 5550 2750
Connection ~ 5550 2750
Wire Wire Line
	5550 2750 6200 2750
Wire Wire Line
	6200 2500 6200 2750
$Comp
L power:GND #PWR?
U 1 1 610116AF
P 4650 4400
AR Path="/6104190B/610116AF" Ref="#PWR?"  Part="1" 
AR Path="/6105FDE6/610116AF" Ref="#PWR0114"  Part="1" 
F 0 "#PWR0114" H 4650 4150 50  0001 C CNN
F 1 "GND" H 4655 4227 50  0000 C CNN
F 2 "" H 4650 4400 50  0001 C CNN
F 3 "" H 4650 4400 50  0001 C CNN
	1    4650 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 4250 4650 4250
Wire Wire Line
	4650 4250 4650 4400
Text GLabel 2500 3050 0    50   Input ~ 0
RFSCK
Text GLabel 2500 2950 0    50   Output ~ 0
RFSDO
Text GLabel 4800 2950 2    50   Input ~ 0
RFSDI
Text GLabel 2500 3150 0    50   Input ~ 0
RFNSEL
Wire Wire Line
	4800 2950 4350 2950
Wire Wire Line
	2950 2950 2500 2950
Wire Wire Line
	2500 3050 2950 3050
Wire Wire Line
	2950 3150 2500 3150
$Comp
L CONUFL001-SMD:CONUFL001-SMD-T J2
U 1 1 6101A8D1
P 2900 5500
F 0 "J2" H 3450 5765 50  0000 C CNN
F 1 "CONUFL001-SMD-T" H 3450 5674 50  0000 C CNN
F 2 "CONUFL001-SMD-T:CONUFL001-SMD" H 3850 5600 50  0001 L CNN
F 3 "https://linxtechnologies.com/wp/product/mhf-rf-connectors/" H 3850 5500 50  0001 L CNN
F 4 "RF Connectors / Coaxial Connectors T&R U.FL Straight Surface Mount Jack" H 3850 5400 50  0001 L CNN "Description"
F 5 "" H 3850 5300 50  0001 L CNN "Height"
F 6 "712-CONUFL001-SMD-T" H 3850 5200 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Linx-Technologies/CONUFL001-SMD-T?qs=EU6FO9ffTwfRdkBeQTdJWQ%3D%3D" H 3850 5100 50  0001 L CNN "Mouser Price/Stock"
F 8 "Linx Technologies" H 3850 5000 50  0001 L CNN "Manufacturer_Name"
F 9 "CONUFL001-SMD-T" H 3850 4900 50  0001 L CNN "Manufacturer_Part_Number"
	1    2900 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 3450 2500 3450
Wire Wire Line
	2500 3450 2500 5500
Wire Wire Line
	2500 5500 2900 5500
$Comp
L power:GND #PWR?
U 1 1 6101D2C2
P 4300 5700
AR Path="/6104190B/6101D2C2" Ref="#PWR?"  Part="1" 
AR Path="/6105FDE6/6101D2C2" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 4300 5450 50  0001 C CNN
F 1 "GND" H 4305 5527 50  0000 C CNN
F 2 "" H 4300 5700 50  0001 C CNN
F 3 "" H 4300 5700 50  0001 C CNN
	1    4300 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 5500 4300 5500
Wire Wire Line
	4300 5500 4300 5600
Wire Wire Line
	4000 5600 4300 5600
Connection ~ 4300 5600
Wire Wire Line
	4300 5600 4300 5700
Text GLabel 2200 3550 0    50   Input ~ 0
RFGPIO0
Text GLabel 2200 3650 0    50   Input ~ 0
RFGPIO1
Text GLabel 2200 3750 0    50   Input ~ 0
RFGPIO2
Text GLabel 2200 3850 0    50   Input ~ 0
RFGPIO3
Text GLabel 2200 3950 0    50   Input ~ 0
RFGPIO4
Text GLabel 2200 4050 0    50   Input ~ 0
RFGPIO5
Wire Wire Line
	2950 3550 2200 3550
Wire Wire Line
	2200 3650 2950 3650
Wire Wire Line
	2950 3750 2200 3750
Wire Wire Line
	2200 3850 2950 3850
Wire Wire Line
	2950 3950 2200 3950
Wire Wire Line
	2200 4050 2950 4050
Text GLabel 2500 3350 0    50   Input ~ 0
RFRESET
Wire Wire Line
	2950 3350 2500 3350
Text GLabel 6650 2500 2    50   Input ~ 0
VCC_RF
Wire Wire Line
	6650 2500 6200 2500
$EndSCHEMATC
